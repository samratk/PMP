>1 Components of integration - Making decisions and trade offs regarding -
[1] Resource allocation - 80-80 sheet
[2] Competing objectives and alternatives.
[3] Interdependencies between the KAs - SMEs
[4] Affordability
[5] Meeting stakeholders expectations.

Integration basically contains crux of monitor and control.

>2 Tracking Tools
  i. Earned value management
  ii. Forecasts
  iii. Quality
  iv. Checklists
  v. Risk Register
  vi. Issue logs
  vii. CCB log

  >3 Direct and Manage Project work
  it is same as project execution.
  Outputs - deliverables, work performance data, change request, proj mgmgmt plan updates, prj doc updates.
