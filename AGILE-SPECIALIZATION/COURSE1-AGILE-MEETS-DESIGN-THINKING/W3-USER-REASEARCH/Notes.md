1. Persona Hypothesis - you can direct or indirect questions.
   1. Questions
      1. Does this person exists
      2. Can you identify them
      3. Do you understand them
      4. What do they think-see-feel-do in your area
   1. Tools
      1. Photos
      2. Think-see-feel-do
      3. Discovery interviews
      4. Story boards
      5. Daily in the life boards

2. Problem Hypothesis
   1. Questions
      1. Have you identified discreet problem needs
      2. How important it is to target persona
      3. What are the alternatives they use today? and How?
   1. Tools
      1. Problem scneario alternatives values
      2. Proportion trios
      3. Discovery interviews
      4. Storyboards.
1. Story
   1. as a personna
   2. What needs to be done
   3. For the reward to attain - A reward is a simple, discrete ending to a user's story. It lets the user know that she got what she wanted. In this case, an alert would let the user know that the inventory has been updated after a new shipment of goods is received.
   4. Example -
      1. Epic - As Ted the HVAC technician, I want to identify a part that needs replacing so I can decide my next steps.
         1. Child story 1 - I know the part number and I want to find it on the system so I can figure out next steps on the repair.
         2. Child story 2 - I dont know the part number and I want to try to identify it online so I can move the job forward.
         3. I want to see the cost of the part
1. Test cases associated to the stories -
   1. Test cases allow teams to identify additional elements of child stories that are worth discussing, a good practice for making sure you record and articulate your ideas for discussion with the rest of the team.
   2. Yes- both of these are relevant. Do try to focus your ideas into user stories and test cases. But if they don't fit and you still think they're important, do not lose them. Write them down in whatever form and make sure you remember to include them in your discussions with the rest of the team. It is best to err on the side of including more details, so the team should make a note of this and other findings so they don't forget to address problems later.


---
3. Value Hypothesis - you cannot ask them about value. You need to design a decision for them. It is different from persona and problem hypothesis. Create a real, valuable decision for the subject that allows you to assess the motivation. It is important to see the choices that user actually makes versus what they say they would choose.
---
4. Customer creation Hypothesis
5. Usability Hypothesis
