1. Positioning Statement -
As a reminder, the format for a complete positioning statement is:
For (target customer) who (statement of the need or opportunity), the (product name) is a (product category) that (statement of key benefit – that is, compelling reason to buy). Unlike (primary alternative), our product (statement of primary differentiation).
