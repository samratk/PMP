1. `I`ndependent
2. `N`egotiable
3. `V`aluable
4. `E`stimable
5. `S`mall
6. `T`estable
---
1. Your stories have all the three clauses.
2. Usability and motivation are both linked with the stories, but they are tested separately.
3. Testing feels easy, frequent, and integral. Each story should have its corresponding test case.
4. Every story is linked with a problem scenario
5. Every story is linked with a value proposition
6. Every story is linked with a vivid persona
7. Collaborating on user stories and driving to a strong shared understanding of them is a crucial element of agile. If you're acting as lead on this, think about ways to engage your team with that material so that they a) become familiar with it and (even more important) b) are a part of creating it. In the team sessions to create the stories, create an agenda that helps the team better understand the user and what constitutes a valuable outcome for them.
8. When coaching your team to create strong user stories, which of the following is most effective? - ask them how to test the reward and outcome clauses of the stories.  Testing is a powerful way to reveal whether or not the narrative is strong and relevant. Teams should be encouraged to ask themselves this question throughout the process.
9. A project manager writes a set of user stories in your issue tracking system, JIRA, and assigns them to developers to begin work. Which element of Bill Wake's INVEST checklist is more immediately applicable to improving this situation? - `NEGOTIABLE` - One of the most important underlying points of 'Negotiable' is that user stories are not a `specification`; they are an `aid` for collaboration. User stories that drive better outcomes are almost always `co-created` by the team through `collaboration` and `story writing workshops`
10. `RED BUTTON DIRECTIVE` - Correct
This is a red button directive because it prescribes an arbitrary implementation to a problem that's not even fully articulated. It's hard for implementers (designers, developers, testers) to bring the best of what they have to offer to a directive like this. A better approach is to describe a testable narrative about what the user is trying to accomplish (with problem scenarios, propositions and user stories).
11. Stories should be - `FOCUSED`, `ACTIONABLE`, `TESTABLE`
