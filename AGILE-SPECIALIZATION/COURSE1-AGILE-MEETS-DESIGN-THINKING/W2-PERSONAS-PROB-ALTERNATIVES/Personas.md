![Userstories](AgileUserStories.png)
![VENTURE](../W1-PROBLEMS-AGILE-SOLVES/The-Venture-design-process.png)

```mermaid
graph LR;
  a(Persona)-->b(problem for user)-->c(User stories)-->d(what is the value as per user)
```
##1. IDENTIFY THE PERSONAS FIRST
This tells us who the user is and what makes them tick.
Personas have to be made such that they are more realistic. And avoid coprorate clichedes.
Think, See, Feel, Do
Empathy and Creativity
What is the kind of shoes they will wear.
1. Vivid
2. Actionable
3. Real
4. Identifiable
5. Exact
6. Detailed
We talked about how personas are a way we humanize our user. And also how we kind of operationalize them in our particular area with this framework of Think, See, Feel, Do. Now we're going to walk through that in a little more detail using an example.
###1.1 What motivates most of our behaviors?
- [ ] Somebody tells us what to do
- [x] We act based on our feelings. We are all emotion-driven creatures who want to be loved.
- [ ] We carefully consider the alaternatives and outcomes
- [ ] We perform quick risk-reward analysis.

##2. DESIGNING USER STORIES
- The desires are relativel more durable and long lasting than solutions. Focus on the parent problem scenarios.
- reach out to the user, and listen to their stories and what-if conditions to come up with "user stories"
- Importance of pivoting oneself to the user needs while defining the story.
![redbutton](redbutton.png)
### One end - Product death cycle. left
![twinantipoles](twin-antipoles.png)
![agile-story.png](agile-story.png)
